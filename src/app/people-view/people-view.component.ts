import { Component, OnInit, Input } from '@angular/core';
import { PeopleTableComponent } from '../people-table/people-table.component';
import { People } from '../classes/people';
import { Place } from '../classes/place';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-people-view',
  templateUrl: './people-view.component.html',
  styleUrls: ['./people-view.component.css']
})
export class PeopleViewComponent implements OnInit {
  @Input() data: any;
  public people: People;
  public parent: PeopleTableComponent;
  public location: Place;
  public model: Place;
  public placeNames: Array<Place>;

  constructor(private _toastrService: NbToastrService) { }

  ngOnInit() {
    this.people = (<People>this.data.data).copyPeople();
    this.parent = <PeopleTableComponent>this.data.parentComponent;
    this.placeNames = new Array<Place>();
    this._getAllPlaces(this.parent.data.place);
    const cur = this.parent.data.current[this.parent.data.current.length - 1];
    this.location = this.parent.data.getPlaceById(cur.id, this.parent.data.place);
  }

  // #region Public Functions
  public updatePeople() {
    if (this.people.name !== '') {
      const invalid = this.people.isValid();
      if (invalid.length === 0) {
        const itemIndex = this.location.people.findIndex(item => item.id === this.people.id);
        if (this.model === undefined || this.location.id === this.model.id) {
          // Same Location
          this.location.people[itemIndex] = this.people;
        } else {
          // Different Location
          this.location.people.splice(itemIndex, 1);
          this.location = this.parent.data.getPlaceById(this.model.id, this.parent.data.place);
          this.location.people.push(this.people);
        }
        this.parent.data.resendPlace();
        this.parent.dialogRef.close();
        this._toastrService.success(this.people.name + ' was edited!', 'Person edited');
      } else {
        this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Person not edited');
      }
    } else {
      this._toastrService.warning('You should give your NPC a name!', 'Person not edited');
    }
  }

  public previous() {
    const itemIndex = this.location.people.findIndex(item => item.id === this.people.id);
    if (itemIndex > 0) {
      this.people = this.location.people[itemIndex - 1].copyPeople();
    } else {
      this.people = this.location.people[this.location.people.length - 1].copyPeople();
    }
  }

  public next() {
    const itemIndex = this.location.people.findIndex(item => item.id === this.people.id);
    if (itemIndex < this.location.people.length - 1) {
      this.people = this.location.people[itemIndex + 1].copyPeople();
    } else {
      this.people = this.location.people[0].copyPeople();
    }
  }

  // Type Ahead Functions
  public searchLocation = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this.placeNames.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )
  public searchFormatter = (x: {name: string}) => x.name;
  // #endregion

  // #region Private Functions
  private _getAllPlaces(place: Place) {
    if (place !== undefined) {
      this.placeNames.push(place);
      place.place.forEach(p => {
        this._getAllPlaces(p);
      });
    }
  }
  // #endregion
}
