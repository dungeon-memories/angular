import { People } from './people';
import { Story } from './story';
import { v4 as uuid } from 'uuid';

export class Place {
    constructor() {
        this.id = uuid();
        this.name = '';
        this.description = '';
        this.place = new Array<Place>();
        this.people = new Array<People>();
        this.story = new Array<Story>();
    }

    id: string;
    name: string;
    description: string;
    place: Place[];
    people: People[];
    story: Story[];

    get placeNumber(): number {
        let i = this.place.length;
        this.place.forEach(p => {
            i += p.placeNumber;
        });
        return i;
    }
    get peopleNumber(): number {
        let i = this.people.length;
        this.place.forEach(p => {
            i += p.peopleNumber;
        });
        return i;
    }
    get storyNumber(): number {
        let i = this.story.length;
        this.place.forEach(p => {
            i += p.storyNumber;
        });
        return i;
    }

    public copyPlace(): Place {
        const p = new Place();
        p.id = this.id;
        p.name = this.name;
        p.description = this.description;
        p.place = this.place;
        p.people = this.people;
        p.story = this.story;
        return p;
    }

    public isValid(): Array<String> {
        const invalid = new Array<String>();
        const check = new Place();
        Object.keys(check).forEach(key => {
            if (typeof check[key] !== typeof this[key]) {
                invalid.push(key);
            }
        });
        return invalid;
    }
}
