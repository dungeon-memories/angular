import { v4 as uuid } from 'uuid';

// #region Random Generator
const name = ['Anbera', 'Artin', 'Audhild', 'Balifra', 'Barbena', 'Bardryn',
    'Bolhild', 'Dagnal', 'Dariff', 'Delre', 'Diesa', 'Eldeth', 'Eridred',
    'Falkrunn', 'Fallthra', 'Finellen', 'Gillydd', 'Gunnloda', 'Gurdis',
    'Helgret', 'Helja', 'Hlin', 'Ilde', 'Jarana', 'Kathra', 'Kilia', 'Kristryd',
    'Liftrasa', 'Marastyr', 'Mardred', 'Morana', 'Nalaed', 'Nora', 'Nurkara',
    'Oriff', 'Ovina', 'Riswynn', 'Sannl', 'Therlin', 'Thodris', 'Torbera',
    'Tordrid', 'Torgga', 'Urshar', 'Valida', 'Vistra', 'Vonana', 'Werydd',
    'Whurdred', 'Yurgunn', 'Adrik', 'Alberich', 'Baern', 'Barendd', 'Beloril',
    'Brottor', 'Dain', 'Dalgal', 'Darrak', 'Delg', 'Duergath', 'Dworic', 'Eberk',
    'Einkil', 'Elaim', 'Erias', 'Fallond', 'Fargrim', 'Gardain', 'Gilthur',
    'Gimgen', 'Gimurt', 'Harbek', 'Kildrak', 'Kilvar', 'Morgran', 'Morkral',
    'Nalral', 'Nordak', 'Nuraval', 'Oloric', 'Olunt', 'Orsik', 'Oskar',
    'Rangrim', 'Reirak', 'Rurik', 'Taklinn', 'Thoradin', 'Thorin', 'Thradal',
    'Tordek', 'Traubon', 'Travok', 'Ulfgar', 'Uraim', 'Veit', 'Vonbin', 'Vondal',
    'Whurbin', 'Adrie', 'Ahinar', 'Althaea', 'Anastrianna', 'Andraste',
    'Antinua', 'Arara', 'Baelitae', 'Bethrynna', 'Birel', 'Caelynn', 'Chaedi',
    'Claira', 'Dara', 'Drusilia', 'Elama', 'Enna', 'Faral', 'Felosial', 'Hatae',
    'Ielenia', 'Ilanis', 'Irann', 'Jarsali', 'Jelenneth', 'Keyleth', 'Leshanna',
    'Lia', 'Maiathah', 'Malquis', 'Meriele', 'Mialee', 'Myathethin', 'Naivara',
    'Quelenna', 'Quillathe', 'Ridaro', 'Sariel', 'Shanairra', 'Shava', 'Silaqui',
    'Summes', 'Theirastra', 'Thiala', 'Tiaathque', 'Traulam', 'Vadania',
    'Valanthe', 'Valna', 'Xanaphia', 'Adran', 'Aelar', 'Aerdeth', 'Ahvain',
    'Aramil', 'Arannis', 'Aust', 'Azaki', 'Beiro', 'Berrian', 'Caeldrim',
    'Carric', 'Dayereth', 'Dreali', 'Efferil', 'Eiravel', 'Enialis', 'Erdan',
    'Erevan', 'Fivin', 'Galinndan', 'Gennal', 'Hadarai', 'Halimath', 'Heian',
    'Himo', 'Immeral', 'Ivellios', 'Korfel', 'Lamlis', 'Laucian', 'Lucan',
    'Mindartis', 'Naal', 'Nutae', 'Paelias', 'Peren', 'Quarion', 'Riardon',
    'Rolen', 'Soveliss', 'Suhnae', 'Thamior', 'Tharivol', 'Theren', 'Theriatis',
    'Thervan', 'Uthemar', 'Vanuath', 'Varis', 'Abalaba', 'Bimpnottin', 'Breena',
    'Buvvie', 'Callybon', 'Caramip', 'Carlin', 'Cumpen', 'Dalaba', 'Donella',
    'Duvamil', 'Ella', 'Ellyjoybell', 'Ellywick', 'Enidda', 'Lilli',
    'Loopmottin', 'Lorilla', 'Luthra', 'Mardnab', 'Meena', 'Menny', 'Mumpena',
    'Nissa', 'Numba', 'Nyx', 'Oda', 'Oppah', 'Orla', 'Panana', 'Pyntle',
    'Quilla', 'Ranala', 'Reddlepop', 'Roywyn', 'Salanop', 'Shamil', 'Siffress',
    'Symma', 'Tana', 'Tenena', 'Tervaround', 'Tippletoe', 'Ulla', 'Unvera',
    'Veloptima', 'Virra', 'Waywocket', 'Yebe', 'Zanna', 'Alston', 'Alvyn',
    'Anverth', 'Arumawann', 'Bilbron', 'Boddynock', 'Brocc', 'Burgell',
    'Cockaby', 'Crampernap', 'Dabbledob', 'Delebean', 'Dimble', 'Eberdeb',
    'Eldon', 'Erky', 'Fablen', 'Fibblestib', 'Fonkin', 'Frouse', 'Frug', 'Gerbo',
    'Gimble', 'Glim', 'Igden', 'Jabble', 'Jebeddo', 'Kellen', 'Kipper',
    'Namfoodle', 'Oppleby', 'Orryn', 'Paggen', 'Pallabar', 'Pog', 'Qualen',
    'Ribbles', 'Rimple', 'Roondar', 'Sapply', 'Seebo', 'Senteq', 'Sindri',
    'Umpen', 'Warryn', 'Wiggens', 'Wobbles', 'Wrenn', 'Zaffrab', 'Zook', 'Houn',
    'Rhivaun', 'Umbril', 'Xaemar', 'Zeltaebar', 'Aali', 'Rashid', 'Tahnon',
    'Tanzim', 'Whalide', 'Aseir', 'Bardeid', 'Haseid', 'Khemed', 'Mehmen',
    'Sudeiman', 'Zasheir', 'Darvin', 'Dorn', 'Evendur', 'Gorstag', 'Grim',
    'Helm', 'Malark', 'Morn', 'Randal', 'Stedd', 'Bor', 'Fodel', 'Glar',
    'Grigor', 'Igan', 'Ivor', 'Kosef', 'Mival', 'Orel', 'Pavel', 'Sergor',
    'Artur', 'Bern', 'Colin', 'Manfred', 'Tristan', 'Boriv', 'Gardar', 'Madevik',
    'Vlad', 'Aldym', 'Chand', 'Meleghost', 'Presmer', 'Sandrue', 'Uregaunt',
    'Ander', 'Blath', 'Bran', 'Frath', 'Geth', 'Lander', 'Luth', 'Malcer',
    'Stor', 'Taman', 'Urth', 'Charva', 'Duma', 'Hukir', 'Jama', 'Pradir',
    'Sikhil', 'Aoth', 'Bareris', 'Ehput-Ki', 'Kethoth', 'Mumed', 'Ramas',
    'So-Kehur', 'Thazar-De', 'Urhur', 'Avan', 'Ostaram', 'Petro', 'Stor',
    'Taman', 'Thalaman', 'Urth', 'Borivik', 'Faurgar', 'Jandar', 'Kanithar',
    'Madislak', 'Ralmevik', 'Shaumar', 'Vladislak', 'Awar', 'Cohis', 'Damota',
    'Gewar', 'Hapah', 'Laskaw', 'Senesaw', 'Tokhis', 'An', 'Chen', 'Chi', 'Fai',
    'Jiang', 'Jun', 'Lian', 'Long', 'Meng', 'On', 'Shan', 'Shui', 'Wen', 'Atlan',
    'Bayar', 'Chingis', 'Chinua', 'Mongke', 'Temur', 'Anton', 'Diero', 'Marcon',
    'Pieron', 'Rimardo', 'Romero', 'Salazar', 'Umbero', 'Amak', 'Chu', 'Imnek',
    'Kanut', 'Siku', 'Glouris', 'Maeve', 'Sevaera', 'Xaemarra', 'Zraela',
    'Aisha', 'Farah', 'Nura', 'Rashida', 'Zalebyeh', 'Atala', 'Ceidil', 'Hama',
    'Jasmal', 'Meilil', 'Seipora', 'Yasheira', 'Zasheida', 'Arveene', 'Esvele',
    'Jhessail', 'Kerri', 'Lureene', 'Miri', 'Rowan', 'Shandri', 'Tessele',
    'Alethra', 'Kara', 'Katernin', 'Mara', 'Natali', 'Olma', 'Tana', 'Zora',
    'Alicia', 'Gennifer', 'Meridith', 'Elaine', 'Olivia', 'Varra', 'Ulmarra',
    'Imza', 'Navarra', 'Yuldra', 'Aithe', 'Chalan', 'Oloma', 'Phaele', 'Sarade',
    'Amafrey', 'Betha', 'Cefrey', 'Kethra', 'Mara', 'Olga', 'Silifrey', 'Westra',
    'Apret', 'Bask', 'Fanul', 'Mokat', 'Nismet', 'Ril', 'Arizima', 'Chathi',
    'Nephis', 'Nulara', 'Murithi', 'Sefris', 'Thola', 'Umara', 'Zolis', 'Anva',
    'Dasha', 'Dima', 'Olga', 'Westra', 'Zlatara', 'Fyevarra', 'Hulmarra',
    'Immith', 'Imzel', 'Navarra', 'Shevarra', 'Tammith', 'Yuldra', 'Anet', 'Bes',
    'Idim', 'Lenet', 'Moqem', 'Neghet', 'Sihvet', 'Bai', 'Chao', 'Jia', 'Lei',
    'Mei', 'Qiao', 'Shui', 'Tai', 'Bolormaa', 'Bortai', 'Erdene', 'Naran',
    'Ulutiun', 'Balama', 'Dona', 'Faila', 'Jalana', 'Luisa', 'Marta', 'Quara',
    'Selise', 'Vonda', 'Akna', 'Chena', 'Kaya', 'Sedna', 'Ublereak'];
const family = ['Aranore', 'Balderk', 'Battlehammer', 'Bigtoe', 'Bloodkith',
    'Bofdann', 'Brawnanvil', 'Brazzik', 'Broodfist', 'Burrowfound', 'Caebrek',
    'Daerdahk', 'Dankil', 'Daraln', 'Deepdelver', 'Durthane', 'Eversharp', 'Fallack',
    'Fireforge', 'Foamtankard', 'Frostbeard', 'Glanhig', 'Goblinbane', 'Goldfinder',
    'Gorunn', 'Graybeard', 'Hammerstone', 'Helcral', 'Holderhek', 'Ironfist',
    'Loderr', 'Lutgehr', 'Morigak', 'Orcfoe', 'Rakankrak', 'Ruby-Eye', 'Rumnaheim',
    'Silveraxe', 'Silverstone', 'Steelfist', 'Stoutale', 'Strakeln', 'Strongheart',
    'Thrahak', 'Torevir', 'Torunn', 'Trollbleeder', 'Trueanvil', 'Trueblood',
    'Ungart', 'Albaratie', 'Bafflestone', 'Beren', 'Boondiggles', 'Cobblelob',
    'Daergel', 'Dunben', 'Fabblestabble', 'Fapplestamp', 'Fiddlefen', 'Folkor',
    'Garrick', 'Gimlen', 'Glittergem', 'Gobblefirn', 'Gummen', 'Horcusporcus',
    'Humplebumple', 'Ironhide', 'Leffery', 'Lingenhall', 'Loofollue', 'Maekkelferce',
    'Miggledy', 'Munggen', 'Murnig', 'Musgraben', 'Nackle', 'Ningel', 'Nopenstallen',
    'Nucklestamp', 'Offund', 'Oomtrowl', 'Pilwicken', 'Pingun', 'Quillsharpener',
    'Raulnor', 'Reese', 'Rofferton', 'Scheppen', 'Shadowcloak', 'Silverthread',
    'Sympony', 'Tarkelby', 'Timbers', 'Turen', 'Umbodoben', 'Waggletop', 'Welber',
    'Wildwander', 'Appleblossom', 'Bigheart', 'Brightmoon', 'Brushgather',
    'Cherrycheeks', 'Copperkettle', 'Deephollow', 'Elderberry', 'Fastfoot',
    'Fatrabbit', 'Glenfellow', 'Goldfound', 'Goodbarrel', 'Goodearth', 'Greenbottle',
    'Greenleaf', 'High-hill', 'Hilltopple', 'Hogcollar', 'Honeypot', 'Jamjar',
    'Kettlewhistle', 'Leagallow', 'Littlefoot', 'Nimblefingers', 'Porridgepot',
    'Quickstep', 'Reedfellow', 'Shadowquick', 'Silvereyes', 'Smoothhands',
    'Stonebridge', 'Stoutbridge', 'Stoutman', 'Strongbones', 'Sunmeadow',
    'Swiftwhistle', 'Tallfellow', 'Tealeaf', 'Tenpenny', 'Thistletop', 'Thorngage',
    'Tosscobble', 'Underbough', 'Underfoot', 'Warmwater', 'Whispermouse',
    'Wildcloak', 'Wildheart', 'Wiseacre', 'Lharaendo', 'Mristar', 'Wyndael', 'Basha',
    'Dumein', 'Jassan', 'Khalid', 'Mostana', 'Pashar', 'Rein', 'Amblecrown',
    'Buckman', 'Dundragon', 'Evenwood', 'Greycastle', 'Tallstag', 'Bersk', 'Chernin',
    'Dotsk', 'Kulenov', 'Marsk', 'Nemetsk', 'Shemov', 'Starag', 'Archer', 'Gareth',
    'Leed', 'Kendrick', 'Morgan', 'Waters', 'Chergoba', 'Drazlad', 'Tazyara',
    'Vargoba', 'Stayankina', 'Avhoste', 'Darante', 'Maurmeril', 'Stamaraster',
    'Brightwood', 'Helder', 'Hornraven', 'Lackman', 'Stormwind', 'Windrivver',
    'Datharathi', 'Melpurvatta', 'Nalambar', 'Tiliputakas', 'Ankhalab', 'Anskuld',
    'Fezim', 'Hahpet', 'Nathandem', 'Sepret', 'Uuthrakt', 'Dashkev', 'Hargroth',
    'Laboda', 'Lackman', 'Stonar', 'Stormwind', 'Sulyma', 'Chergoba', 'Dyernina',
    'Iltazyara', 'Murnyethara', 'Stayanoga', 'Ulmokina', 'Cor', 'Marak', 'Laumee',
    'Harr', 'Moq', 'Qo', 'Harr', 'Woraw', 'Tarak', 'Chien', 'Huang', 'Kao', 'Kung',
    'Lao', 'Ling', 'Mei', 'Pin', 'Shin', 'Sum', 'Tan', 'Wan', 'Agosto', 'Astorio',
    'Calabra', 'Domine', 'Falone', 'Marivaldi', 'Pisacar', 'Ramondo'];
const races = ['Dragonborn', 'Dwarf', 'Elf', 'Gnome', 'Halfling',
    'Half-Elf', 'Half-Orc', 'Human', 'Tiefling', 'Aarakocra', 'Genasi',
    'Goliath', 'Aasimar', 'Firbolg', 'Lizardfolk', 'Tabaxi', 'Tortle'];
const colors = ['#800000', '#FF0000', '#FFA500', '#FFFF00', '#808000',
    '#008000', '#800080', '#FF00FF', '#00FF00', '#008080', '#00FFFF', '#0000FF', '#000080', '#000000', '#808080', '#C0C0C0', '#FFFFFF'];
const length = ['None', 'Very Short', 'Short', 'Medium', 'Long', 'Very Long'];
const ages = ['Very Young', 'Young', 'Middle Aged', 'Older', 'Old', 'Very Old'];
const weights = ['Severely underweight', 'Underweight', 'Slim', 'Normal', 'Slightly Overweight', 'Overweight', 'Obese'];
const appearance = ['Flamboyant or outlandish clothes',
    'Distinctive jewelry: earrings, necklace, circlet, bracelets',
    'Piercings', 'Formal, clean clothes', 'Ragged, dirty clothes',
    'Pronounced scar', 'Missing teeth', 'Missing fingers',
    'Unusual eye color (or two different colors)',
    'Tattoos', 'Birthmark', 'Unusual skin color', 'Bald',
    'Braided beard or hair', 'Unusual hair color', 'Nervous eye twitch',
    'Distinctive nose', 'Distinctive posture (crooked or rigid)',
    'Exceptionally beautiful', 'Exceptionally ugly'
];
const bonds = ['Dedicated to fulfilling a personal life goal',
    'Protective of close family members',
    'Protective of colleagues or compatriots',
    'Loyal to a benefactor, patron, or employer',
    'Captivated by a romantic interest', 'Drawn to a special place',
    'Protective of a sentimental keepsake',
    'Protective of a valuable possession', 'Out for revenge',
    'Roll twice, ignoring results of 10'];
const flaws = ['Forbidden love or susceptibility to romance',
    'Enjoys decadent pleasures', 'Arrogance',
    'Envies another creature\'s possessions or station',
    'Overpowering greed', 'Prone to rage', 'Has a powerful enemy',
    'Specific phobia', 'Shameful or scandalous history',
    'Secret crime or misdeed', 'Possession of forbidden lore',
    'Foolhardy bravery'];
const ability_high = ['Strength-powerful, brawny, strong as an ox',
    'Dexterity-lithe, agile, graceful', 'Constitution-hardy, hale, healthy',
    'Intelligence-studious, learned, inquisitive',
    'Wisdom-perceptive, spiritual, insightful',
    'Charisma-persuasive, forceful, born leader'];
const ability_low = ['Strength-feeble, scrawny', 'Dexterity-clumsy, fumbling',
    'Constitution-sickly, pale', 'Intelligence-dim-witted, slow',
    'Wisdom-oblivious, absentminded', 'Charisma-dull, boring'];
const talents = ['Plays a musical instrument',
    'Speaks several languages fluently', 'Unbelievably lucky',
    'Perfect memory', 'Great with animals', 'Great with children',
    'Great at solving puzzles', 'Great at one game', 'Great at impersonations',
    'Draws beautifully', 'Paints beautifully', 'Sings beautifully',
    'Drinks everyone under the table', 'Expert carpenter', 'Expert cook',
    'Expert dart thrower and rock skipper', 'Expert juggler',
    'Skilled actor and master of disguise', 'Skilled dancer',
    'Knows thieves\' cant'];
const manners = ['Prone to singing, whistling, or humming quietly',
    'Speaks in rhyme or some other peculiar way',
    'Particularly low or high voice', 'Slurs words, lisps, or stutters',
    'Enunciates overly clearly', 'Speaks loudly', 'Whispers',
    'Uses flowery speech or long words', 'Frequently uses the wrong word',
    'Uses colorful oaths and exclamations', 'Makes constant jokes or puns',
    'Prone to predictions of doom', 'Fidgets', 'Squints',
    'Stares into the distance', 'Chews something', 'Paces',
    'Taps fingers', 'Bites fingernails', 'Twirls hair or tugs beard'];
const traits = ['Argumentative', 'Arrogant', 'Blustering', 'Rude',
    'Curious', 'Friendly', 'Honest', 'Hot tempered', 'Irritable',
    'Ponderous', 'Quiet', 'Suspicious'];
const ideals = ['Beauty (Good)', 'Charity (Good)', 'Greater Good (Good)',
    'Life (Good)', 'Respect (Good)', 'Self-sacrifice (Good)',
    'Domination (Evil)', 'Greed (Evil)', 'Might (Evil)', 'Pain (Evil)',
    'Retribution (Evil)', 'Slaughter (Evil)', 'Community (Lawful)',
    'Fairness (Lawful)', 'Honor (Lawful)', 'Logic (Lawful)',
    'Responsibility (Lawful)', 'Tradition (Lawful)', 'Change (Chaotic)',
    'Creativity (Chaotic)', 'Freedom (Chaotic)', 'Independence (Chaotic)',
    'No limits (Chaotic)', 'Whimsy (Chaotic)', 'Balance (Neutral)',
    'Knowledge (Neutral)', 'Live and let live (Neutral)', 'Moderation (Neutral)',
    'Neutrality (Neutral)', 'People (Neutral)', 'Aspiration (Any)',
    'Discovery (Any)', 'Glory (Any)', 'Nation (Any)', 'Redemption (Any)',
    'Self-knowledge (Any)'];
// #endregion

export class People {
    constructor() {
        this.id = uuid();
        this.name = name[Math.floor(Math.random() * name.length)] + ' ' + family[Math.floor(Math.random() * family.length)];
        this.race = races[Math.floor(Math.random() * races.length)];
        this.gender = (Math.random() >= 0.5) ? 'Female' : 'Male';
        this.age = (Math.random() >= 0.75) ? 'Unknown' : ages[Math.floor(Math.random() * ages.length)];
        this.height = length[Math.floor(Math.random() * length.length)];
        this.weight = weights[Math.floor(Math.random() * weights.length)];
        this.eyes = colors[Math.floor(Math.random() * colors.length)];
        this.hair_color = colors[Math.floor(Math.random() * colors.length)];
        this.hair_length = length[Math.floor(Math.random() * length.length)];
        this.appearance = appearance[Math.floor(Math.random() * appearance.length)];
        this.ability_high = ability_high[Math.floor(Math.random() * ability_high.length)];
        this.ability_low = ability_low[Math.floor(Math.random() * ability_low.length)];
        this.talent = talents[Math.floor(Math.random() * talents.length)];
        this.mannerism = manners[Math.floor(Math.random() * manners.length)];
        this.trait = traits[Math.floor(Math.random() * traits.length)];
        this.ideal = ideals[Math.floor(Math.random() * ideals.length)];
        this.bond = bonds[Math.floor(Math.random() * bonds.length)];
        this.flaw = flaws[Math.floor(Math.random() * flaws.length)];
        this.priority = 0;
        this.alive = 'Alive';
        this.description = '';
    }

    id: string;
    name: string; // Important
    race: string; // Important
    gender: string; // Important
    age: string; // Important
    height: string;
    weight: string;
    eyes: string;
    hair_color: string;
    hair_length: string;
    appearance: string;
    ability_high: string;
    ability_low: string;
    talent: string;
    mannerism: string;
    trait: string;
    ideal: string;
    bond: string;
    flaw: string;
    priority: number; // Important
    alive: string; // Important
    description: string;

    public copyPeople(): People {
        const p = new People();
        p.id = this.id;
        p.name = this.name;
        p.race = this.race;
        p.gender = this.gender;
        p.age = this.age;
        p.height = this.height;
        p.weight = this.weight;
        p.eyes = this.eyes;
        p.hair_color = this.hair_color;
        p.hair_length = this.hair_length;
        p.appearance = this.appearance;
        p.ability_high = this.ability_high;
        p.ability_low = this.ability_low;
        p.talent = this.talent;
        p.mannerism = this.mannerism;
        p.trait = this.trait;
        p.ideal = this.ideal;
        p.bond = this.bond;
        p.flaw = this.flaw;
        p.priority = this.priority;
        p.alive = this.alive;
        p.description = this.description;
        return p;
    }

    public isValid(): Array<String> {
        const invalid = new Array<String>();
        const check = new People();
        Object.keys(check).forEach(key => {
            if (typeof check[key] !== typeof this[key]) {
                invalid.push(key);
            }
        });
        return invalid;
    }
}
