import { v4 as uuid } from 'uuid';
export class Story {
    constructor() {
        this.id = uuid();
        this.name = '';
        this.date = '';
        this.content = '';
        this.people = new Array<string>();
    }

    id: string;
    name: string;
    date: string;
    content: string;
    people: string[];   // By ID

    get contentNumber(): number {
        if (this.content === undefined) {
            return 0;
        } else {
            return this.content.length;
        }
    }

    get peopleNumber(): number {
        return this.people.length;
    }

    public copyStory(): Story {
        const s = new Story();
        s.id = this.id;
        s.name = this.name;
        s.date = this.date;
        s.content = this.content;
        s.people = [...this.people];
        return s;
    }

    public isValid(): Array<String> {
        const invalid = new Array<String>();
        const check = new Story();
        Object.keys(check).forEach(key => {
            if (typeof check[key] !== typeof this[key]) {
                invalid.push(key);
            }
        });
        return invalid;
    }
}
