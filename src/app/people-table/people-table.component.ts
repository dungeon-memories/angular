import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { CampaignDataService } from '../campaign-data/campaign-data.service';
import { Ng2SmartTableComponent } from 'ng2-smart-table/ng2-smart-table.component';
import { People } from './../classes/people';
import { NbDialogService, NbDialogRef, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-people-table',
  templateUrl: './people-table.component.html',
  styleUrls: ['./people-table.component.css']
})
export class PeopleTableComponent implements OnInit, AfterViewInit {
  @ViewChild('table') smartTable: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: any;
  // @ViewChild('tooltip') tooltip: any;
  // public tooltiptext: string;
  public dialogRef: NbDialogRef<any>;
  public source: Array<People>;
  public settings: Object;

  constructor(public data: CampaignDataService,
    private _dialogService: NbDialogService,
    private _toastrService: NbToastrService) {
    this.source = new Array();
    const updateSourceCallback = (): void => {
      this.updateSource();
    };
    this.data.addCurrentFunction(updateSourceCallback);
    this.data.addPlaceFunction(updateSourceCallback);
  }

  ngOnInit() {
    this.settings = {
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        name: {
          title: 'Name',
          editor: {
            type: 'text',
          },
        },
        description: {
          title: 'Description',
          editor: {
            type: 'textarea',
          },
        },
        race: {
          title: 'Race',
          editor: {
            type: 'text',
          },
          width: '20px',
        },
        gender: {
          title: 'Gender',
          editor: {
            type: 'text',
          },
          width: '20px',
        },
        age: {
          title: 'Age',
          editor: {
            type: 'text',
          },
          width: '20px',
        },
        priority: {
          title: 'Priority',
          editor: {
            type: 'text',
          },
          width: '20px',
        },
        alive: {
          title: 'Alive',
          editor: {
            type: 'checkbox',
            config: {
              true: 'Alive',
              false: 'Dead'
            },
          },
          width: '20px',
        },
      },
    };
    this.updateSource();
  }

  ngAfterViewInit(): void {
    // Create new Item
    this.smartTable.createConfirm.subscribe((event) => {
      const people = this._rowToPeople(event.newData);
      if (people.name !== '') {
        const invalid = people.isValid();
        if (invalid.length === 0) {
          const latest = this.data.current[this.data.current.length - 1];
          const obj = this.data.getPlaceById(latest.id, this.data.place);
          obj.people.push(people);
          this.data.resendPlace();
          this._toastrService.success(people.name + ' was added to the list!', 'Person added');
        } else {
          this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Person not added');
        }
      } else {
        this._toastrService.warning('You should give your NPCs a name!', 'Person not added');
      }
    });
    // Edit Item
    this.smartTable.editConfirm.subscribe((event) => {
      const people = this._rowToPeople(event.newData);
      if (people.name !== '') {
        const invalid = people.isValid();
        if (invalid.length === 0) {
          const latest = this.data.current[this.data.current.length - 1];
          const parent = this.data.getPlaceById(latest.id, this.data.place);
          const itemIndex = parent.people.findIndex(item => item.id === people.id);
          parent.people[itemIndex] = people;
          this.data.resendPlace();
          this._toastrService.success(people.name + ' was edited!', 'Person edited');
        } else {
          this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Person not edited');
        }
        // Toast with success
      } else {
        this._toastrService.warning('You should give your NPCs a name!', 'Person not edited');
      }
    });
    // Delete Item
    this.smartTable.deleteConfirm.subscribe((event) => {
      const people = <People>event.data;
      const latest = this.data.current[this.data.current.length - 1];
      const parent = this.data.getPlaceById(latest.id, this.data.place);
      const itemIndex = parent.people.findIndex(item => item.id === people.id);
      parent.people.splice(itemIndex, 1);
      this.data.resendPlace();
      this._toastrService.info(people.name + ' left the Campaign!', 'Person deleted');
    });
    // Navigate
    this.smartTable.userRowSelect.subscribe((event) => {
      // Open Markdown
      const wrapper = {
        parentComponent: this,
        data: event.data,
      };
      this.dialogRef = this._dialogService.open(this.dialog, {
        context: wrapper
      });
    });
    // this.smartTable.rowHover.subscribe((event) => {
    //   this.tooltip.open();
    //   this.tooltiptext = event.data.description;
    // });
  }

  public updateSource() {
    this.smartTable.initGrid();
    if (this.data.current.length > 0) {
      const cur = this.data.current;
      this.source = cur[cur.length - 1].people;
      console.log('Update People Table Source', this.source);
    }
  }

  private _rowToPeople(data: People): People {
    const s = new People();
    Object.keys(s).forEach(key => {
      if (data[key] !== undefined && data[key] !== '') {
        if (key !== 'priority') {
          s[key] = data[key];
        } else {
          s[key] = parseInt(data[key.toString()], 0);
        }
      }
    });
    return s;
  }
}
