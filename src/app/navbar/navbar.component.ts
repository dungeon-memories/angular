import { People } from './../classes/people';
import { HomeComponent } from './../home/home.component';
import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CampaignDataService } from '../campaign-data/campaign-data.service';
import { saveAs } from 'file-saver';
import { Place } from '../classes/place';
import { Story } from '../classes/story';

class SearchModel {
  id: string;
  type: string;
  name: string;
  description: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input() parent: HomeComponent;
  public model: any;
  public hidden: boolean;

  private _smodel: Array<SearchModel>;
  private _spath: Array<Place>;

  constructor(
    private router: Router,
    private data: CampaignDataService,
  ) {
    const updateSourceCallback = (): void => {
      this.createSearchList();
    };
    this.data.addPlaceFunction(updateSourceCallback);
  }

  ngOnInit() {
    this.hidden = false;
  }

  // #region Public
  public home() {
    if (this.data.initialized) {
      this.router.navigateByUrl('/');
      window.location.reload();
    }
  }

  public toggle() {
    if (this.data.initialized) {
      this.parent.toggle();
    }
  }

  public save() {
    if (this.data.initialized) {
      const json = this._escape(JSON.stringify(this.data.place));
      saveAs(new Blob([json]), this.data.place.name + '.json');
    }
  }

  public hide() {
    this.hidden = !this.hidden;
    this.parent.hide();
  }

  public navigate(item: SearchModel) {
    this._spath = new Array<Place>();
    const p = this.data.getPlaceById(item.id, this.data.place);
    this._findPathToPlace(this.data.place, p);
    this.data.updateCurrent(this._spath.reverse());
    this.model = null;
  }

  // Search
  public createSearchList() {
    this._smodel = new Array<SearchModel>();
    this._createSearchList(this.data.place);
  }

  public search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this._smodel.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )

  public searchFormatter = (x: {name: string}) => x.name;
  // #endregion

  // #region Private
  private _findPathToPlace(root: Place, target: Place): Place {
    if (root.id === target.id) {
      this._spath.push(root);
      return target;
    }
    let place: Place;
    root.place.forEach(p => {
      const temp = this._findPathToPlace(p, target);
      if (temp !== undefined) {
        place = temp;
        this._spath.push(root);
      }
    });
    return place;
  }

  private _createSearchList(place: Place) {
    this._smodel.push(this._placeToSearch(place));
    place.people.forEach(person => {
      const sm = this._personToSearch(person);
      sm.id = place.id;
      this._smodel.push(sm);
    });
    place.story.forEach(story => {
      const sm = this._storyToSearch(story);
      sm.id = place.id;
      this._smodel.push(sm);
    });
    place.place.forEach(p => {
      this._createSearchList(p);
    });
  }

  private _placeToSearch(place: Place): SearchModel {
    const sm = new SearchModel();
    sm.id = place.id;
    sm.name = place.name;
    sm.type = 'Place';
    sm.description = place.description;
    return sm;
  }

  private _personToSearch(person: People): SearchModel {
    const sm = new SearchModel();
    sm.name = person.name;
    sm.type = 'Person';
    sm.description = person.description;
    return sm;
  }

  private _storyToSearch(story: Story): SearchModel {
    const sm = new SearchModel();
    sm.name = story.name;
    sm.type = 'Story';
    sm.description = story.date;
    return sm;
  }

  private _escape(str: string): string {
    return escape(str);
  }
  // #endregion
}
