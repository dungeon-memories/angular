import { Component, OnInit } from '@angular/core';
import { CampaignDataService } from '../campaign-data/campaign-data.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  constructor(public data: CampaignDataService) {}

  ngOnInit() {
  }

  public navigate(index: number) {
    const newcurrent = this.data.current.slice(0, index + 1);
    this.data.updateCurrent(newcurrent);
  }
}
