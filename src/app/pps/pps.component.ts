import { Component, OnInit } from '@angular/core';
import { CampaignDataService } from '../campaign-data/campaign-data.service';
import { Place } from '../classes/place';

@Component({
  selector: 'app-pps',
  templateUrl: './pps.component.html',
  styleUrls: ['./pps.component.css']
})
export class PpsComponent implements OnInit {
  public tapview: boolean;
  public empty: boolean; // Hide empty taps and views

  constructor(public data: CampaignDataService) {
  }

  ngOnInit() {
    this.tapview = true;
    this.empty = false;
  }

  // #region Public Functions
  public toggleView() {
    this.tapview = !this.tapview;
  }

  public toggleEmpty() {
    console.log(this.empty);
    this.empty = !this.empty;
  }

  public reloadData() {
    this.data.resendPlace();
  }

  public combineRecursive() {
    if (this.data.initialized) {
      this.data.toggleRecursive();
    }
  }
  // #endregion

  // #region Getter
  get place(): Place {
    return this.data.current[this.data.current.length - 1];
  }

  get placeNumber(): number {
    return this.data.recursive ? this.place.placeNumber : this.place.place.length;
  }

  get peopleNumber(): number {
    return this.data.recursive ? this.place.peopleNumber : this.place.people.length ;
  }

  get storyNumber(): number {
    return this.data.recursive ? this.place.storyNumber : this.place.story.length;
  }

  get emptyPlaces(): boolean {
    if (this.empty) {
      if (this.place.place.length === 0) {
        return false;
      }
    }
    return true;
  }

  get emptyPeople(): boolean {
    if (this.empty) {
      if (this.place.people.length === 0) {
        return false;
      }
    }
    return true;
  }

  get emptyStory(): boolean {
    if (this.empty) {
      if (this.place.story.length === 0) {
        return false;
      }
    }
    return true;
  }
  // #endregion

}
