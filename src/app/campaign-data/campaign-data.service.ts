import { Story } from './../classes/story';
import { Place } from '../classes/place';
import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { People } from '../classes/people';

@Injectable({
  providedIn: 'root'
})
export class CampaignDataService {
  private _initialized: boolean;
  private _recursive: boolean;

  private _placeFunctions: Array<Function>;
  private _currentFunctions: Array<Function>;

  private _current: Place[];
  private _place: Place;

  constructor(private router: Router) {
    this._initialized = false;
    this._recursive = false;
    this._current = new Array<Place>();
    this._placeFunctions = new Array<Function>();
    this._currentFunctions = new Array<Function>();
  }

  //#region Public Functions
  public jsonToPlace(json: string) {
    const res = this._initObject(JSON.parse(json));
    this.updatePlace(res);
    this._initialized = true;

    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe(() => {
      console.log('Router Event:', this.router.url);
      this._updateRoute();
    });
  }

  public addPlaceFunction(f: Function) {
    this._placeFunctions.push(f);
  }

  public addCurrentFunction(f: Function) {
    this._currentFunctions.push(f);
  }

  public updatePlace(p: Place) {
    if (JSON.stringify(p) !== JSON.stringify(this.place)) {
      console.log('Update Place:', p);
      this._place = p;
      this._updateRoute();
      this.resendPlace();
    }
  }

  public resendPlace() {
    const path = decodeURI(this.router.url).split('/').reverse();
    path.pop();
    const p = this._pathToPlaces(path, this.place);
    this._current = p;
    this._placeFunctions.forEach(f => {
      f();
    });
  }

  public updateCurrent(p: Place[]) {
    if (JSON.stringify(this.current) !== JSON.stringify(p)) {
      console.log('Update Current:', p);
      this._current = p;
      this.resendCurrent();
    }
  }

  public toggleRecursive() {
    this._recursive = !this.recursive;
  }

  public resendCurrent() {
    this._currentFunctions.forEach(f => {
      f();
    });
    this.router.navigate(this.current.map(function(place) {
      return place.name;
    }));
  }

  public getPlaceById(id: string, root: Place): Place {
    if (root.id === id) {
      return root;
    }
    let place: Place;
    root.place.forEach(p => {
      const temp = this.getPlaceById(id, p);
      if (temp !== undefined) {
        place = temp;
      }
    });
    return place;
  }
  //#endregion

  //#region Getters
  public get place(): Place {
    return this._place;
  }

  public get current(): Place[] {
    return this._current;
  }

  public get initialized(): boolean {
    return this._initialized;
  }

  public get recursive(): boolean {
    return this._recursive;
  }
  //#endregion

  //#region Privates
  private _updateRoute() {
    const path = decodeURI(this.router.url).split('/').reverse();
    path.pop(); // Remove Root
    if (path.length > 0 && path[0] !== '') {
      console.log('Update Route', path);
      const p = this._pathToPlaces(path, this.place);
      this.updateCurrent(p);
    } else {
      this.updateCurrent([this.place]);
    }
  }

  private _initObject(place: Place): Place {
    const p = new Place();
    // Basic Init with uuid support
    if (place.id === undefined) {
      p.id = uuid();
    } else {
      p.id = place.id;
    }
    p.name = place.name;
    p.description = place.description;

    // People Init
    p.people = new Array<People>();
    place.people.forEach(pe => {
      const pep = new People();
      Object.keys(pe).forEach(key => {
        pep[key] = pe[key];
      });
      p.people.push(pep);
    });

    // Story Init
    p.story = new Array<Story>();
    place.story.forEach(st => {
      const story = new Story();
      Object.keys(st).forEach(key => {
        story[key] = st[key];
      });
      p.story.push(story);
    });

    // Place Init
    p.place = new Array<Place>();
    place.place.forEach(pl => {
      p.place.push(this._initObject(pl));
    });

    return p;
  }

  private _pathToPlaces(paths: Array<String>, node: Place): Array<Place> {
    const ar = new Array<Place>();
    const path = paths.pop();
    if (node !== undefined && path === node.name) {
      ar.push(node);
      node.place.forEach(c => {
        const path2 = [...paths];
        const ray = this._pathToPlaces(path2, c);
        ray.forEach(content => {
          ar.push(content);
        });
      });
    }
    return ar;
  }
  //#endregion
}
