import { CampaignDataService } from './../campaign-data/campaign-data.service';
import { Place } from '../classes/place';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TreeComponent, TreeNode } from 'angular-tree-component';
import * as _ from 'lodash';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.css']
})
export class ExplorerComponent implements OnInit {
  public nodes: Array<Object>;
  public options: Object;
  public selected_node: TreeNode;
  public selected_height: number;
  public selected_depth: number;

  @ViewChild(TreeComponent)
  private tree: TreeComponent;

  constructor(public data: CampaignDataService) {
    // Set up data for tree
    this.data = data;
    this.options = {
      useVirtualScroll: true,
      allowDrag: (node) => !node.isRoot,
      actionMapping: {
        mouse: {
          click: (tree, node: TreeNode, $event) => {
            this._click(node);
          },
          dblClick: (tree, node: TreeNode, $event) => {
            this._recursiveExpand(node, !node.isExpanded);
          },
        }
      }
    };
  }

  ngOnInit() {
    if (this.data.current.length > 0) {
      this._delayUpdate(1);
    }
  }

  private _timeout(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  private async _delayUpdate(ms: number) {
    const root = this.tree.treeModel.getFirstRoot();
    await this._timeout(ms);
    if (root !== undefined) {
      const place = this.data.current[this.data.current.length - 1];
      this._click(this.tree.treeModel.getNodeById(place.id));
    } else {
      this._delayUpdate(ms * 2);
    }
  }

  public initializeTree() {
    // Initialize Reverse Listeners for Campaign Tree
    const updatePlaceCallback = (): void => {
      this.updateTree();
    };
    this.data.addPlaceFunction(updatePlaceCallback);
    // Initialize Reverse Listeners for Current Selected
    const updateCurrentCallback = (): void => {
      this.updateCurrent();
    };
    this.data.addCurrentFunction(updateCurrentCallback);
    // Initialize Real Data
    this.updateTree();
  }

  public updateTree() {
    const newnodes = this._build(this.data.place);
    if (JSON.stringify([newnodes]) === JSON.stringify(this.nodes)) {
      return;
    }
    console.log('Update Tree', newnodes);
    this.nodes = new Array();
    this.nodes.push(newnodes);
    this.tree.treeModel.update();
    // const first = this.tree.treeModel.getFirstRoot();
    // if (first !== undefined) {
    //   this.data.updatePlace(this._nodeToPlace(first));
    //   return true;
    // } else {
    //   return false;
    // }
  }

  public moveNode() {
    const newnodes = this._build(this.data.place);
    this.nodes = new Array();
    this.nodes.push(newnodes);
    // this.tree.treeModel.update();
    const first = this.tree.treeModel.getFirstRoot();
    this.data.updatePlace(this._nodeToPlace(first));
  }

  public updateCurrent() {
    const cur = this.data.current;
    const node = <TreeNode>this.tree.treeModel.getNodeById(cur[cur.length - 1].id);
    this._click(node);
  }

  public delete_selected() {
    if (this.selected_height > 1) {
      const p = this.selected_node.parent;
      _.remove(this.selected_node.parent.data.children, this.selected_node.data);
      this.tree.treeModel.update();
      this.data.updatePlace(this._nodeToPlace(this.tree.treeModel.getFirstRoot()));
      this._click(p);
    }
  }

  private _nodeToPlace(node: TreeNode): Place {
    const n = node.data;
    const old = this._getPlaceById(n['id'], this.data.place);
    const p = new Place();
    p.id = n.id;
    p.name = n['name'];
    p.description = old.description;
    p.people = old.people;
    p.story = old.story;
    p.place = new Array<Place>();
    node.children.forEach(child => {
      p.place.push(this._nodeToPlace(child));
    });
    return p;
  }

  private _getPlaceById(id: string, place: Place): Place {
    if (place.id !== id) {
      let find: Place;
      place.place.forEach(p => {
        const foo = this._getPlaceById(id, p);
        if (foo !== undefined) {
          find = foo;
        }
      });
      return find;
    } else {
      return place;
    }
  }

  private _click(node: TreeNode) {
    node.setIsActive(true);
    node.setActiveAndVisible(true);
    const n = <TreeNode>this.tree.treeModel.getNodeById(node.id);
    this.selected_node = n;
    const path = this._getPath(n);
    this.selected_height = path.length;
    this.selected_depth = this._getDepth(n, 1);
    this.data.updateCurrent(path);
  }

  private _recursiveExpand(node: TreeNode, expand: boolean) {
    node.children.forEach(child => {
      this._recursiveExpand(child, expand);
    });
    node.setIsExpanded(expand);
  }

  private _getPath(node: TreeNode): Array<Place> {
    const ray = new Array<Place>();
    while (!node.isRoot) {
      ray.push(this._nodeToPlace(node));
      node = node.parent;
    }
    ray.push(this._nodeToPlace(node));
    return ray.reverse();
  }

  private _getDepth(node: TreeNode, depth: number): number {
    let d = 0;
    node.children.forEach(child => {
      const cd = this._getDepth(child, depth);
      if (d < cd) {
        d = cd;
      }
    });
    depth += d;
    return depth;
  }

  private _build(p: Place): Object {
    const n = {};
    n['name'] = p.name;
    n['id'] = p.id;
    n['children'] = new Array();
    p.place.forEach(place => {
      n['children'].push(this._build(place));
    });
    return n;
  }
}
