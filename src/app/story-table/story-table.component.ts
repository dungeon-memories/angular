import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { CampaignDataService } from '../campaign-data/campaign-data.service';
import { Ng2SmartTableComponent } from 'ng2-smart-table/ng2-smart-table.component';
import { v4 as uuid } from 'uuid';
import { Story } from '../classes/story';
import { NbDialogService, NbDialogRef, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-story-table',
  templateUrl: './story-table.component.html',
  styleUrls: ['./story-table.component.css']
})
export class StoryTableComponent implements OnInit, AfterViewInit {
  @ViewChild('table') smartTable: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: any;
  public dialogRef: NbDialogRef<any>;
  public source: Array<Story>;
  public settings: Object;

  constructor(public data: CampaignDataService,
    private _dialogService: NbDialogService,
    private _toastrService: NbToastrService) {
    this.source = new Array();
    const updateSourceCallback = (): void => {
      this.updateSource();
    };
    this.data.addCurrentFunction(updateSourceCallback);
    this.data.addPlaceFunction(updateSourceCallback);
  }

  ngOnInit() {
    this.settings = {
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        name: {
          title: 'Name',
          type: 'string',
          editable: true,
          addable: true,
        },
        date: {
          title: 'Date',
          type: 'string',
          editable: true,
          addable: true,
        },
        contentNumber: {
          title: 'Content',
          type: 'number',
          editable: false,
          addable: false,
          width: '20px',
        },
        peopleNumber: {
          title: 'People',
          type: 'number',
          editable: false,
          addable: false,
          width: '20px',
        },
      },
    };
    this.updateSource();
  }

  ngAfterViewInit(): void {
    // Create new Item
    this.smartTable.createConfirm.subscribe((event) => {
      const story = this._rowToStory(event.newData);
      if (story.name !== '') {
        const invalid = story.isValid();
        if (invalid.length === 0) {
          const latest = this.data.current[this.data.current.length - 1];
          const obj = this.data.getPlaceById(latest.id, this.data.place);
          obj.story.push(story);
          this.data.resendPlace();
          this._toastrService.success(story.name + ' was added to the list!', 'Story added');
        } else {
          this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Story not added');
        }
      } else {
        this._toastrService.warning('You should give your Story a name!', 'Story not added');
      }
    });
    // Edit Item
    this.smartTable.editConfirm.subscribe((event) => {
      const story = this._rowToStory(event.newData);
      if (story.name !== '') {
        const invalid = story.isValid();
        if (invalid.length === 0) {
          const latest = this.data.current[this.data.current.length - 1];
          const parent = this.data.getPlaceById(latest.id, this.data.place);
          const itemIndex = parent.story.findIndex(item => item.id === story.id);
          parent.story[itemIndex] = story;
          this.data.resendPlace();
          this._toastrService.success(story.name + ' was edited!', 'Story edited');
        } else {
          this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Story not edited');
        }
      } else {
        this._toastrService.warning('You should give your Story a name!', 'Story not edited');
      }
    });
    // Delete Item
    this.smartTable.deleteConfirm.subscribe((event) => {
      const story = <Story>event.data;
      const latest = this.data.current[this.data.current.length - 1];
      const parent = this.data.getPlaceById(latest.id, this.data.place);
      const itemIndex = parent.story.findIndex(item => item.id === story.id);
      parent.story.splice(itemIndex, 1);
      this.data.resendPlace();
      this._toastrService.info(story.name + ' left the Campaign!', 'Person deleted');
    });
    // Navigate
    this.smartTable.userRowSelect.subscribe((event) => {
      // Open Markdown
      const wrapper = {
        parentComponent: this,
        data: event.data,
      };
      this.dialogRef = this._dialogService.open(this.dialog, {
        context: wrapper
      });
    });
  }

  public updateSource() {
    this.smartTable.initGrid();
    if (this.data.current.length > 0) {
      const cur = this.data.current;
      this.source = cur[cur.length - 1].story;
      console.log('Update Story Table Source', this.source);
    }
  }

  private _rowToStory(data: Story): Story {
    const s = new Story();
    if (data.id === undefined) {
      s.id = uuid();
    } else {
      s.id = data.id;
    }
    if (data.name !== undefined) {
      s.name = data.name;
    }
    if (data.date !== undefined) {
      s.date = data.date;
    }
    if (data.content !== undefined) {
      s.content = data.content;
    }
    if (data.people !== undefined) {
      s.people = data.people;
    }
    return s;
  }
}
