import { CampaignDataService } from './../campaign-data/campaign-data.service';
import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Place } from '../classes/place';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {
  public uploader: FileUploader;
  public name: string;
  public description: string;

  constructor(public data: CampaignDataService) { }

  ngOnInit() {
    this.uploader = new FileUploader({});
  }

  public upload() {
    const reader = new FileReader();
    reader.onload = (_) => {
      if (reader.result) {
        const base64result = (<string>reader.result).split(',')[1];
        this.data.jsonToPlace(unescape(atob(base64result)));
      }
    };
    reader.readAsDataURL(this.uploader.queue[0]._file);
  }

  public create() {
    const root = new Place();
    root.name = this.name;
    root.description = this.description;
    this.data.jsonToPlace(JSON.stringify(root));
  }
}
