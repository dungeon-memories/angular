import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { CampaignDataService } from '../campaign-data/campaign-data.service';
import { Ng2SmartTableComponent } from 'ng2-smart-table/ng2-smart-table.component';
import { v4 as uuid } from 'uuid';
import { Place } from '../classes/place';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-place-table',
  templateUrl: './place-table.component.html',
  styleUrls: ['./place-table.component.css']
})
export class PlaceTableComponent implements OnInit, AfterViewInit {
  @ViewChild('table') smartTable: Ng2SmartTableComponent;
  public source: Array<Place>;
  public settings: Object;

  constructor(public data: CampaignDataService,
    private _toastrService: NbToastrService) {
    this.source = new Array();
    const updateSourceCallback = (): void => {
      this.updateSource();
    };
    this.data.addCurrentFunction(updateSourceCallback);
    this.data.addPlaceFunction(updateSourceCallback);
  }

  ngOnInit() {
    this.settings = {
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        name: {
          title: 'Name',
          type: 'text',
          editable: true,
          addable: true,
          width: '20%',
        },
        description: {
          title: 'Description',
          type: 'text',
          editable: true,
          addable: true,
          width: '60%',
          editor: {
            type: 'textarea',
          },
        },
        placeNumber: {
          title: 'Places',
          type: 'text',
          editable: false,
          addable: false,
          width: '20px',
        },
        peopleNumber: {
          title: 'People',
          type: 'text',
          editable: false,
          addable: false,
          width: '20px',
        },
        storyNumber: {
          title: 'Stories',
          type: 'text',
          editable: false,
          addable: false,
          width: '20px',
        },
      },
    };
    this.updateSource();
  }

  ngAfterViewInit(): void {
    // Create new Item
    this.smartTable.createConfirm.subscribe((event) => {
      const place = this._rowToPlace(event.newData);
      if (place.name !== '') {
        const invalid = place.isValid();
        if (invalid.length === 0) {
          const latest = this.data.current[this.data.current.length - 1];
          const obj = this.data.getPlaceById(latest.id, this.data.place);
          obj.place.push(place);
          this.data.resendPlace();
          this._toastrService.success(place.name + ' was added to the list!', 'Place added');
        } else {
          this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Place not added');
        }
      } else {
        this._toastrService.warning('You should give your place a name!', 'Place not added');
      }
    });
    // Edit Item
    this.smartTable.editConfirm.subscribe((event) => {
      const place = this._rowToPlace(event.newData);
      if (place.name !== '') {
        const invalid = place.isValid();
        if (invalid.length === 0) {
          const latest = this.data.current[this.data.current.length - 1];
          const parent = this.data.getPlaceById(latest.id, this.data.place);
          const itemIndex = parent.place.findIndex(item => item.id === place.id);
          parent.place[itemIndex] = place;
          this.data.resendPlace();
          this._toastrService.success(place.name + ' was edited!', 'Place edited');
        } else {
          this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Place not edited');
        }
      } else {
        this._toastrService.warning('You should give your place a name!', 'Place not edited');
      }
    });
    // Delete Item
    this.smartTable.deleteConfirm.subscribe((event) => {
      const place = <Place>event.data;
      const latest = this.data.current[this.data.current.length - 1];
      const parent = this.data.getPlaceById(latest.id, this.data.place);
      const itemIndex = parent.place.findIndex(item => item.id === place.id);
      parent.place.splice(itemIndex, 1);
      this.data.resendPlace();
      this._toastrService.info(place.name + ' disappeared from the map!', 'Place deleted');
    });
    // Navigate
    this.smartTable.userRowSelect.subscribe((event) => {
      const place = <Place>event.data;
      this.data.current.push(this.data.getPlaceById(place.id, this.data.place));
      this.data.resendCurrent();
    });
  }

  public updateSource() {
    this.smartTable.initGrid();
    if (this.data.current.length > 0) {
      const cur = this.data.current;
      this.source = cur[cur.length - 1].place;
      console.log('Update Place Table Source', this.source);
    }
  }

  private _rowToPlace(data: Place): Place {
    const p = new Place();
    if (data.id !== undefined) {
      p.id = data.id;
    }
    p.name = data.name;
    p.description = data.description;
    if (data.place !== undefined) {
      p.place = data.place;
    }
    if (data.people !== undefined) {
      p.people = data.people;
    }
    if (data.story !== undefined) {
      p.story = data.story;
    }
    return p;
  }
}
