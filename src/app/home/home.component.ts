import { CampaignDataService } from './../campaign-data/campaign-data.service';
import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public self: HomeComponent;
  public hidden: boolean;

  constructor(
    public data: CampaignDataService,
    private sidebarService: NbSidebarService) {
  }

  ngOnInit() {
    this.self = this;
    this.hidden = false;
  }

  public toggle() {
    this.sidebarService.toggle(true, 'left');
  }

  public hide() {
    this.hidden = !this.hidden;
  }
}
