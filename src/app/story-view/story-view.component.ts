import { Component, OnInit, Input } from '@angular/core';
import { Story } from '../classes/story';
import { StoryTableComponent } from '../story-table/story-table.component';
import { People } from '../classes/people';
import { Place } from '../classes/place';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-story-view',
  templateUrl: './story-view.component.html',
  styleUrls: ['./story-view.component.css']
})
export class StoryViewComponent implements OnInit {
  @Input() data: any;
  public story: Story;
  public parent: StoryTableComponent;
  public location: Place;
  public peopleModel: People;
  public locationModel: Place;
  public peopleNames: Array<People>;
  public placeNames: Array<Place>;

  constructor(private _toastrService: NbToastrService) { }

  ngOnInit() {
    this.story = (<Story>this.data.data).copyStory();
    this.parent = <StoryTableComponent>this.data.parentComponent;
    this.peopleNames = new Array<People>();
    this.placeNames = new Array<Place>();
    this._getAllPeople(this.parent.data.place);
    this._getAllPlaces(this.parent.data.place);
    const cur = this.parent.data.current[this.parent.data.current.length - 1];
    this.location = this.parent.data.getPlaceById(cur.id, this.parent.data.place);
  }

  // #region Public Functions
  public updateStory() {
    if (this.story.name !== '') {
      const invalid = this.story.isValid();
      if (invalid.length === 0) {
        const itemIndex = this.location.story.findIndex(item => item.id === this.story.id);
        if (this.locationModel === undefined || this.location.id === this.locationModel.id) {
          this.location.story[itemIndex] = this.story;
        } else {
          this.location.story.splice(itemIndex, 1);
          this.location = this.parent.data.getPlaceById(this.locationModel.id, this.parent.data.place);
          this.location.story.push(this.story);
        }
        this.parent.data.resendPlace();
        this.parent.dialogRef.close();
        this._toastrService.success(this.story.name + ' was edited!', 'Story edited');
      } else {
        this._toastrService.danger('Wrong types for filed(s): ' + invalid.join(',') + '!', 'Story not edited');
      }
    } else {
      this._toastrService.warning('You should give your Story a name!', 'Story not edited');
    }
  }

  public addPerson(person: People) {
    if (person !== undefined && person !== null && person.id !== undefined) {
      if (!(this.story.people.find(p => p === person.id))) {
        this.story.people.push(person.id);
        this._toastrService.danger('Person added to Story!', 'Person added');
      }
      this.peopleModel = undefined;
    } else {
      this._toastrService.danger('Person not found!', 'Person not added');
    }
  }

  public getP(id: string): People {
    if (id !== undefined) {
      return this.peopleNames.find(p => p.id === id);
    } else {
      return new People();
    }
  }

  public removePerson(id: string) {
    if (id !== undefined) {
      const index = this.story.people.findIndex(p => p === id);
      this.story.people.splice(index, 1);
      this._toastrService.danger('Person removed from Story!', 'Person removed');
    } else {
      this._toastrService.danger('Person not found!', 'Person not removed');
    }
  }

  // Type Ahead Functions
  public storyPerson = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this.peopleNames.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )
  public searchFormatter = (x: {name: string}) => x.name;

  public searchLocation = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this.placeNames.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )
  // #endregion

  // #region Private Functions
  private _getAllPeople(place: Place) {
    if (place !== undefined) {
      place.people.forEach(person => {
        this.peopleNames.push(person);
      });
      place.place.forEach(p => {
        this._getAllPeople(p);
      });
    }
  }

  private _getAllPlaces(place: Place) {
    this.placeNames.push(place);
    place.place.forEach(p => {
      this._getAllPlaces(p);
    });
  }
  // #endregion
}
