import { CampaignDataService } from './campaign-data/campaign-data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TreeModule } from 'angular-tree-component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxMdModule } from 'ngx-md';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { FileUploadModule } from 'ng2-file-upload';
import { ColorPickerModule } from 'ngx-color-picker';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbLayoutModule,
  NbMenuModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbBadgeModule,
  NbInputModule,
  NbAccordionModule,
  NbListModule,
  NbUserModule,
  NbCheckboxModule,
  NbPopoverModule,
  NbContextMenuModule,
  NbProgressBarModule,
  NbStepperModule,
  NbDialogModule,
  NbToastrModule
} from '@nebular/theme';

import { HomeComponent } from './home/home.component';
import { ExplorerComponent } from './explorer/explorer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { PpsComponent } from './pps/pps.component';
import { PeopleViewComponent } from './people-view/people-view.component';
import { PeopleTableComponent } from './people-table/people-table.component';
import { StoryViewComponent } from './story-view/story-view.component';
import { StoryTableComponent } from './story-table/story-table.component';
import { PlaceTableComponent } from './place-table/place-table.component';
import { UploaderComponent } from './uploader/uploader.component';
import { FooterComponent } from './footer/footer.component';

const NB_MODULES = [
  NbButtonModule,
  NbCardModule,
  NbLayoutModule,
  NbTabsetModule,
  NbInputModule,
  NbAccordionModule,
  NbListModule,
  NbRouteTabsetModule,
  NbMenuModule,
  NbBadgeModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbCheckboxModule,
  NbPopoverModule,
  NbContextMenuModule,
  NbStepperModule,
  NgbModule,
  NbProgressBarModule,
  NbDialogModule
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ExplorerComponent,
    NavbarComponent,
    BreadcrumbComponent,
    PpsComponent,
    PeopleViewComponent,
    PeopleTableComponent,
    StoryViewComponent,
    StoryTableComponent,
    PlaceTableComponent,
    UploaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    Ng2SmartTableModule,
    TextareaAutosizeModule,
    FileUploadModule,
    ColorPickerModule,
    ...NB_MODULES,
    NgxMdModule.forRoot(),
    TreeModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    NbThemeModule.forRoot({ name: 'default' })
  ],
  providers: [
    CampaignDataService,
    NbMenuModule.forRoot().providers
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
