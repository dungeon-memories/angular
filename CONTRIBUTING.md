
# How can I contribute?

## Development Setup
To get started developing yourself you will need:
* A good IDE like [Visual Studio Code](https://code.visualstudio.com/)
* [Node 8](https://nodejs.org)
* [Angular CLI](https://cli.angular.io/)

## Improve documentation

As a user you are the perfect candidate to help us improve our documentation. Open issues for the things that need improvement and make suggestions how to apply them. Creating Wikis and helping other users is one of the top priority.

## Improve issues

Some issues are created with missing information, not reproducible, or plain invalid. Help make them easier to resolve. Handling issues takes a lot of time that we could rather spend on fixing bugs and adding features.

## Write code

You know web development and angular? Help us improve the code:
* [Documentation]() as I said, can always be improved.
* [Style](): Make it look more like a dungeon while keeping the usability.
* [Features]() are always welcome. Adding a simple Color Picker or a quality of life change.
* [Bug](): We try to avoid this but sometimes we just can't.
* [Testing]() would be really nice. The angular CLI provides tools like E2E testing which I never really got in touch with.

Please think twice before adding new new dependencies or updating existing ones. The latest is version is not always the best.


## Submitting an issue

- The issue tracker is for issues.
- Search the issue tracker before opening an issue.
- Use a clear and descriptive title.
- Include as much information as possible: Steps to reproduce the issue, error message, browser type and version, etc.


## Submitting a merge request

- Non-trivial changes are often best discussed in an issue first, to prevent you from doing unnecessary work.
- Create a branch 'feature/`yourFeature`' and do your work there.
- For ambitious tasks, you should try to get your work in front of the community for feedback as soon as possible. Open a change request as soon as you have done the minimum needed to demonstrate your idea. At this early stage, don't worry about making things perfect, or 100% complete. Add a [WIP] prefix to the title, and describe what you still need to do. This lets reviewers know not to nit-pick small details or point out improvements you already know you need to make.
- New features should be accompanied with tests and documentation if applicable.
- Right now the Pipeline will lint your changes but thats basically everything right now. 
- Use a clear and descriptive title for the merge request and commits.
- You might be asked to do changes to your merge request. Keep Working on your branch and try again!