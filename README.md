Dungeon Memories
==============================
> A tool to store all your awesome stories you and your players experienced in your world. 

[![build status](https://gitlab.com/dungeon-memories/angular/badges/master/build.svg)](https://gitlab.com/dungeon-memories/angular/commits/master)
[![documentation status](https://dungeon-memories.gitlab.io/angular/images/coverage-badge-documentation.svg)](https://dungeon-memories.gitlab.io/angular/coverage.html)


Get started and create your own campaign or use an existing one!

![https://dm.domnick.io](src/assets/android-chrome-256x256.png)

Contributing
------------
[Contribution Guide](https://gitlab.com/dungeon-memories/angular/blob/master/CONTRIBUTING.md)

Versioning
----------
Given a version number MAJOR.MINOR.PATCH, increment the:

1. MAJOR version when you make incompatible backwards-compatible changes,
1. MINOR version when you add functionality in a backwards-compatible
   manner, and
1. PATCH version when you make backwards-compatible bug fixes.

Credits
-------
* [Angular Bootstrap](https://ng-bootstrap.github.io)
* [Nebular](https://akveo.github.io)
* [NG2-Smart-Table](https://akveo.github.io/ng2-smart-table/)
* [Angular Tree Component](https://angular2-tree.readme.io/)
* [NG2-File-Upload](https://valor-software.com/ng2-file-upload/)
* [FileSaver](https://github.com/eligrey/FileSaver.js)
* [NgxMd](https://dimpu.github.io/ngx-md/)
* [Ngx-Textarea-Autosize](https://github.com/evseevdev/ngx-textarea-autosize)
* [Compodoc](https://compodoc.app/)

License
-------
[MIT](https://gitlab.com/dungeon-memories/angular/blob/master/LICENSE)